Test pre-requisites:
Install Java 8. 
Install firefox and Eclipse.

My enivornment is 32-bits, but it may be also good to run in 64-bits environment.


Test script files:
I exported the project in both archive file format(atlassian.jira.tar), and jar file (atlassian.jira.jar).
You should be able to import either of them into Eclipse to run.


How to run:
After importing the files into Eclipse, you should see a project named 'jira'.
Select src/test/java/ atlassina.jira/TestJiraCreateIssue and run as JUnit test.